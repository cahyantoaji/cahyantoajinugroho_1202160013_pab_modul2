package com.example.android.myapplication;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import java.sql.Time;
import java.time.Year;
import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    Switch switch1;
    TextView tgl_brkt,tgl_plg, jam_plg, jam_brkt, saldo;
    Button topup;
    Spinner spin;
    String tujuan, harga;
    int tahun,bulan,hari,jam,menit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saldo = findViewById(R.id.saldo);

        topup = findViewById(R.id.topup);
        topup.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final Dialog dialog_topup = new Dialog(MainActivity.this);

                dialog_topup.setContentView(R.layout.topup);

                final EditText saldotambah = dialog_topup.findViewById((R.id.isiSaldo));
                Button cancel = dialog_topup.findViewById(R.id.btnCancel);
                Button topup = dialog_topup.findViewById(R.id.btntopup);

                topup.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int tambahan = Integer.parseInt(saldotambah.getText().toString());
                        int saldo_current = Integer.parseInt(saldo.getText().toString());
                        int total = saldo_current + tambahan;

                        saldo.setText(String.valueOf(total));

                        dialog_topup.dismiss();
                    }
                });

                cancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog_topup.dismiss();
                    }
                });

                dialog_topup.show();

            }
        });

        spin = findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> spinadapt = ArrayAdapter.createFromResource(this,R.array.tampil_array,R.layout.support_simple_spinner_dropdown_item);
        spinadapt.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(spinadapt);
        spin.setOnItemSelectedListener(new PilihSpin());

        jam_plg = findViewById(R.id.jam_plg);
        tgl_plg = findViewById(R.id.tgl_plg);
        switch1 = findViewById(R.id.switch1);

        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked ) {
                if(switch1.isChecked()) {
                    tgl_plg.setVisibility(View.VISIBLE);
                    jam_plg.setVisibility(View.VISIBLE);

                } else {
                    tgl_plg.setVisibility(View.INVISIBLE);
                    jam_plg.setVisibility(View.INVISIBLE);
                }

            }
        });

        tgl_brkt = findViewById(R.id.tgl_brkt);
        tgl_brkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                tahun = c.get(Calendar.YEAR);
                bulan = c.get(Calendar.MONTH);
                hari = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tgl_brkt.setText(dayOfMonth + "/" + (month+1) + "/" + year);
                    }
                },tahun,bulan,hari);
                dpd.show();

            }
        });


        tgl_plg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                tahun = c.get(Calendar.YEAR);
                bulan = c.get(Calendar.MONTH);
                hari = c.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dpd = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        tgl_plg.setText(dayOfMonth + "/" + (month+1) + "/" + year);
                    }
                },tahun,bulan,hari);
                dpd.show();

            }
        });

        jam_brkt = findViewById(R.id.jam_brkt);
        jam_brkt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                tahun = c.get(Calendar.YEAR);
                bulan = c.get(Calendar.MONTH);
                hari = c.get(Calendar.DAY_OF_MONTH);

                TimePickerDialog tpd = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        jam_brkt.setText(hourOfDay + "," + minute);
                    }


                },jam,menit,false);
                tpd.show();

            }
        });

        jam_plg = findViewById(R.id.jam_plg);
        jam_plg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                tahun = c.get(Calendar.YEAR);
                bulan = c.get(Calendar.MONTH);
                hari = c.get(Calendar.DAY_OF_MONTH);

                TimePickerDialog tpd = new TimePickerDialog(MainActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        jam_brkt.setText(hourOfDay + "," + minute);
                    }
                },jam,menit,false);
                tpd.show();

            }
        });
    }

    private class PilihSpin implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            position = parent.getSelectedItemPosition();

            String[] tujuan_array = getResources().getStringArray(R.array.tujuan_array);
            tujuan = String.valueOf(tujuan_array[position]);

            String[] harga_array = getResources().getStringArray(R.array.tujuan_array);
            harga = String.valueOf(tujuan_array[position]);

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    }
}
